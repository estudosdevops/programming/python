## Repositorio para estudos sobre Python:

Este repositorio é para anotações sobre meus estudos iniciais sobre a linguagem python.

## Funções:

Funções utilizadas no python como built-in
+ `print()` Utilizanda para mostrar menssagens
+ `len()` Informa o tamanho da senquencia de caracteres emnom uma variavel
+ `type()` Informa o tipo da variavel

## Modulos
 + `math`: Utilizado para operações matematicas mais complexas

## Variaveis:
 + São dinamicas
 + Não aceita numeros, espaço.
 + Sempre criar variaveis auto explicativo.
 + Exemplos:
     - `minha_idade` = 17
     - `nome` = 'Fabio'

## Tipo de dados: String
Os tipos de dados strings são textos em variaves, os operadores que podem ser utilizados são:
 + `'+'` para realizar a concatenação
 + `'*'` para realizar muiltiplicação
 + Indice indica as letras em variavel exemplos:
    - `nome[0]`   mostra a primeira letra
    - `nome[-1]`  mostra a ultima letra
    - `nome[0:2]` mostra as duas primeiras letras
    - `nome[-1:]` mostra ultima letra
    - `name[:2]`  mostra as duas primeira letras
+ Exemplo de concatenação
     ```py
     email = nome + '.' + sobrenome_pai + '@' + empresa + '.com (exemplo de concatenação)
     ```
## Tipos de dados: Numeros
 + Para fazer soma precisa colocar os numeros em () ex: (5 + 2) /2
 + Comparação entre numeros ex: 2 > 5
 + Exemplo de utilização dos operadores

 ```py
 if idade >= 18:
     print('Acesso liberado')
else: print('Acesso negado')
```
## Tipos de dados: Dicionarios

Um dicionario tambem é um conjunto de valores, assim como listas/tuplas, com a diferença que não são ordenadas e sim com chave/valor, tambem são elemento mutavel
+ Exemplos
     - `fabio = {'nome': 'fabio','sobrenome': 'pereira','profissão': 'programador python','filhos': ['pedro','maria']}`
     - `fabio['sobrenome']` | consulta o dict
     - `len(fabio)` | verifica o tamanho do dict
     - `del fabio['filhos']` | apaga um valor no dict
     - `fabio ['profissão']` = 'aposentado' | alterar chave no dict
     - `'sobrenome' in fabio` | consulta uma chave no dict
     - `fabio.get('nome', 'Esta infomação não consta no cadastro')` | utilzando o metodo get para pegar informações no dict
     - `fabio['filhos'].append('jorge')` | add elemento na lista dentro do dict

+ Exemplo de loop em dict

```py
for x in fabio:
     print(x)

for x in fabio:
print(x+': '+fabio[x])
```
          
## Listas e tuplas:

 Tuplas imutáveis e tambem sequencias assim como strings.
+ O python sabe quando é uma tupla quando inicia com ()
+ Exemplos:
     - `meses = ('janeiro','fevereiro','março','abril','junho')`

Listas mutáveis e tambem sequencias, de nomes e numeros assim como strings,
+ Python conhece listas quando se inicia entre [].
+ Exemplos:
     - `alunos = ['joao','maria','pedro','helena']`
     - `alunos[1] = 'mariah'` | muda um alemneto dentro da lista
 + Metodos: para manipular as listas

     - `alunos.append('fabio')` | add um novo elemento na lista
     - `alunos.insert(1,'paula')` | add um elemento no meio da lista
     - `alunos.sort()` | coloca lista em ordem alfabetica
     - `alunos.pop(1)` | remove um elemento da lista pelo indice
     - `alunos.remove('fabio')` | remove um elemento da lista pelo nome
     - `SLICING`
          - Fatiamento significa extrair apenas uma parte da string, ou seja, uma substring. Com essa operação, podemos delimitar os limites inferior e superior do pedaço da string que queremos acessar

## Condionais
Operadores Condicionais do Python. Os Operadores Condicionais são utilizados para fazer as comparações dos valores que são passados e retornam o valor Verdadeiro ou Falso que são considerados tipos de dados

 - Tabela Operador Tipo Valor
     - `== Igualdade`    Verifica a igualdade entre dois valores.
     - `!= Igualdade`  	Verifica a diferença entre dois valores.
     - `>  Comparação`	Verificar se o valor A é maior que o valor B.
     - `<  Comparação`	Verifica se o valor A é menor que o valor B.
     - `>= Comparação`	Verifica se o valor A é maior ou igual ao valor B.
     - `<= Comparação`	Verifica se o valor A é menor ou igual ao valor B.
     - `In Seqüência`    Verifica se o valor A está contido em um conjunto.

## Loops
Existem dois principais tipos de loops no Python, for, while.
 - `while` Significa enquanto
 - Comandos que podemos usar em um loop
     - `continue` ; A instrução continue interrompe a execução do ciclo sem interromper a execução do laço de repetição.
     - `break` ; A instrução break finaliza a iteração e o Script continua a execução normalmente.
 - Exemplos
     - [loops](https://gitlab.com/estudosdevops/courses/python/blob/master/myapps/loops.py)

## Funções
Funções são utilizadas para guardar um bloco de codigo para que possa ser utilizados em qualquer lugar dentro do nosso programa.

## Links para Consulta:

+ [keyword-list](https://www.programiz.com/python-programming/keyword-list)
+ [modulo-math](https://docs.python.org/2/library/math.html)
+ [modulos buil in](https://docs.python.org/3/py-modindex.html)
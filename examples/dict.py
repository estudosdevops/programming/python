cores = {'verde': 'green', 'vermelho': 'red', 'preto': 'black', 'branco': 'white'}
cor = input('Informe a cor que deseja traduzir: ').lower()
traducao = cores.get(cor, 'Esta cor não consta no meu dicionario')
print(traducao)
# Exemplo for loop com range
for i in range(0,10):
    print(10-i)

# Exemplo de loop for aninhado
compras = [['arroz','feijão','macarrão'],['carne','frango','peixe'],['leite','iorgute']]

for i in compras:
    for item in i:
        print(item)

# Exemplo loop for com dicionario
cores = {'verde': 'green','preto': 'black', 'azul': 'blue'}

for i in cores:
    print(i,':',cores[i])


# Exemplo com somatorio com loop for
vendas = [1000,450,300,920,600]
total = 0

for i in vendas:
    total += i
    print(total)

# Exemplo loop for com listas e strings
compras = ['arroz','feijão','macarrão','carne']
nome = 'joaquim'

for i in nome:
    print(i)

# Exemplo loop while
x = 0
pessoas = []

while x < 4:
    nome = input('Qual é o seu nome? ')
    ## Evitar que o nome João seja adicionado a lista
    if nome == 'joao': 
        break
    pessoas.append(nome)
    x += 1
print(pessoas)
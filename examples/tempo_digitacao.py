import time
import matplotlib.pyplot as plt

tempos = []
vezes = []
legenda = []
vez = 1
repet = 3

print('Este programa marcará o tempo gasto para digitar a palavra FABIO. Você terá que digita-la '+str(repet)+ 'vezes')
input('Aperte enter para começar.')

while vez <= repet:
    inicio = time.clock()
    input('Digite a palavra: ')
    fim = time.clock()
    tempo = round(fim-inicio,2)
    tempos.append(tempo)
    vezes.append(vez)
    vezstr = str(vez)+'a vez'
    legenda.append(vezstr)
    vez += 1

plt.xticks(vezes,legenda)
plt.plot(vezes,tempos)
plt.show()
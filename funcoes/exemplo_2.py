"""Serie de Funções #2."""

anomnima = lambda param: param + 2
anomnima_plus = lambda param1, param2: param1 + param2

def soma_posicional(x, y):
    """x e y são parametros posicionais."""
    return  x + y

def soma_nomeados(x=7, y=7):
    """
    x e y são parametros nomeados.
    na falta de x ou y o valor 7 sera usado.
    """
    print(f'x: {x}, y:{y}')
    return x + y

def soma_explicitamente_nomeados(*, x=7, y=7):
    """
    x e y são parametros nomeados.
    na falta de x ou y o valor 7 sera usado.
    """
    print(f'x: {x}, y:{y}')
    return x + y

def soma_explicitamente_nomeados2(x=7, *, y=7):
    """
    x e y são parametros nomeados.
    na falta de x ou y o valor 7 sera usado.
    """
    print(f'x: {x}, y:{y}')
    return x + y

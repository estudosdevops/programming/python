"""Anotações de tipo.

Pesquisar Depois:
- PEP-483.
- mypy
- monkeytype
"""

from numbers import Number
from typing import Union, Any, List, Dict, Sequence

somavel = Union[Number, str, list]

def soma(x: somavel, y: somavel) -> somavel:
    return x + y


def identidade(val: Any) -> Any:
    return val

def meu_sum(l: List[Number]) -> Number:
    return sum(l)

def cadastro_usuario(
    nome: str,
    idade: int,
    gostos: List[str]
) -> Dict[str, Union[str, int, List[str]]]:
    return {
        'nome': nome,
        'idade': idade,
        'gostos': gostos,
    }

def meu_min(seq: Sequence):
    return min(seq)
